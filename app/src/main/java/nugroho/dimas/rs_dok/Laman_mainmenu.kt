package nugroho.dimas.rs_dok

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_laman_mainmenu.*

class Laman_mainmenu : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_laman_mainmenu)

        showAbout.setOnClickListener(this)
        showDoc.setOnClickListener(this)
        showRs.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.showDoc->{
             var aa = Intent(this,list_doc_sp::class.java)
                startActivity(aa)
            }

            R.id.showRs->{
                var aa = Intent(this,halaman_Rs::class.java)
                startActivity(aa)
            }
            R.id.showAbout->{
                Toast.makeText(this,"Menu About",Toast.LENGTH_LONG).show()
            }
        }
    }
}
