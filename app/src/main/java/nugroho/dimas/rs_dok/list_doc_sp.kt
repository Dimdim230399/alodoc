package nugroho.dimas.rs_dok

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_halaman_doc.*
import kotlinx.android.synthetic.main.activity_list_doc_sp.*
import org.json.JSONArray

class list_doc_sp : AppCompatActivity(), View.OnClickListener {

   // var url = "http://192.168.43.156/coba_Laravel7/public/dokter/getDataDokterSp"
   var url ="http://192.168.43.126/dashboard/coba_Laravel7/public/dokter/getDataDokterSp"
    var nama : String = ""
    var rcres = 100
    var dataDoc = mutableListOf<HashMap<String,String>>()
    lateinit var adapterDoc: AdapterDoc
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_doc_sp)

        adapterDoc = AdapterDoc(dataDoc,this)
        rcPildocSp.layoutManager = GridLayoutManager(this,2)
        rcPildocSp.adapter = adapterDoc

        crDok.setOnClickListener(this)
        crspdok.setOnClickListener(this)

        var paket : Bundle? = intent.extras
        nama = paket?.getString("nama").toString()
        if(nama != null){
            ShowDok("",nama)
        }else {
            ShowDok("", "")
        }
    }



    fun ShowDok(namaDoc : String, namaSp : String){
        val request = object : StringRequest(
            Request.Method.POST,url, Response.Listener { response ->
                dataDoc.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("nm_doc",jsonObject.getString("DOKTER_NAMA"))
                    info.put("sp_nm",jsonObject.getString("nama_sp"))
                    info.put("alamat",jsonObject.getString("RS_NAMA"))
                    info.put("url",jsonObject.getString("DOKTER_GBR"))
                    info.put("profil",jsonObject.getString("DOKTER_PROFIL"))
                    info.put("dokstr",jsonObject.getString("DOKTER_STR"))
                    info.put("doc_hp",jsonObject.getString("DOKTER_HP"))
                    info.put("rs_gam",jsonObject.getString("url"))

                    dataDoc.add(info)
                }
                adapterDoc.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama_doc",namaDoc)
            hm.put("nama_sp",namaSp)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.crDok->{
                ShowDok(inpdok.text.toString().trim(),"")

                var paket : Bundle? = intent.extras
                nama = paket?.getString("nama").toString()
                Toast.makeText(this, nama , Toast.LENGTH_SHORT).show()
            }
            R.id.crspdok->{
                var aa = Intent(this , halaman_doc::class.java)
                startActivityForResult(aa,rcres)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
            if (resultCode == rcres){
                nama = data?.extras?.getString("nama").toString()
                ShowDok("",nama)
            }
    }


}
