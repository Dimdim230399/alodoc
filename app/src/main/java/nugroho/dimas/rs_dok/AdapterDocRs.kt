package nugroho.dimas.rs_dok

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterDocRs (val dataDocter : List<HashMap<String,String>>, val halamanDocSp: list_doc_rs) : RecyclerView.Adapter<AdapterDocRs.HolderRs> (){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDocRs.HolderRs {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.isi_doc,parent,false)
        return HolderRs(v)
    }

    override fun getItemCount(): Int {
        return dataDocter.size
    }

    override fun onBindViewHolder(holder: AdapterDocRs.HolderRs, position: Int) {
        val data = dataDocter.get(position)
        holder.txNamaRs.setText(data.get("nm_doc"))
        holder.txsp.setText(data.get("sp_nm"))
        holder.txalm.setText(data.get("alamat"))
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.gam)

        if (position.rem(2) == 0) holder.car.setBackgroundColor(Color.rgb(230,245,240))
        else holder.car.setBackgroundColor(Color.rgb(255,255,245))
        val set1: Animation = AnimationUtils.loadAnimation(this.halamanDocSp, R.anim.rcanim)
        holder.car.startAnimation(set1)


        holder.car.setOnClickListener {
            val det =  Intent(this.halamanDocSp, profil_doc::class.java)
            det.putExtra("nama",dataDocter.get(position).get("nm_doc"))
            det.putExtra("spesialis",dataDocter.get(position).get("sp_nm"))
            det.putExtra("nama_rs",dataDocter.get(position).get("alamat"))
            det.putExtra("foto_doc",dataDocter.get(position).get("url"))
            det.putExtra("profil_doc",dataDocter.get(position).get("profil"))
            det.putExtra("str",dataDocter.get(position).get("dokstr"))
            det.putExtra("nohp",dataDocter.get(position).get("doc_hp"))
            det.putExtra("foto_rs",dataDocter.get(position).get("rs_gam"))
            this.halamanDocSp.startActivity(det)
        }

    }


    class HolderRs(v : View) : RecyclerView.ViewHolder(v){
        val txNamaRs = v.findViewById<TextView>(R.id.tvNamaDoc)
        val txsp = v.findViewById<TextView>(R.id.txsp)
        val txalm = v.findViewById<TextView>(R.id.tvAlm)
        val gam=v.findViewById<ImageView>(R.id.foto)
        val  car = v.findViewById<CardView>(R.id.CardDoc)
    }


}